package com.mortgage.serviceimpl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.mortgage.dao.MortgageDao;
import com.mortgage.domain.Offer;
import com.mortgage.domain.User;
import com.mortgage.domain.UserOffer;
import com.mortgage.service.MortgageService;

@RunWith(MockitoJUnitRunner.class)
public class MortgageServiceImplTest {

	@InjectMocks
	MortgageService mortgageService = new MortgageServiceImpl();

	@Mock
	MortgageDao mortgageDao;

	User user;

	UserOffer userOffer;
	
	Offer offer;

	@Before
	public void setUp() {

		user = new User();
		user.setUserid(101);
		user.setAddress("Bangalore");
		user.setDob("12-10-88");
		user.setExistingEmi("120000");
		user.setFamilyIncome("700000");
		user.setFullName("Saurabh");
		user.setGender("male");
		user.setOtherIncome("200000");
		user.setSalary("50000");
		user.setPan("fdjsafjh");
		user.setUsername("saurabhk");
		user.setPassword("1234");
		
		userOffer = new UserOffer();
		offer = new Offer();
	}

	@Test
	public void testRetriveUserByLoginCerdentialPass() {
		Mockito.when(mortgageDao.findUserDetails("saurabhk", "1234")).thenReturn(user);
		User retriveUser = mortgageService.findUserDetails("saurabhk", "1234");
		assertNotNull(retriveUser);
	}

	@Test
	public void testRetriveUserByLoginCerdentialFail() {
		Mockito.when(mortgageDao.findUserDetails("kumar", "abc")).thenReturn(user);
		User retriveUser = mortgageService.findUserDetails("kumar", "abc");
		assertNotNull(retriveUser);
	}

	@Test
	public void testMortgageOfferByUserIncomeMoreThanFiftyThousand() {
		List<String> retriveOffer = mortgageService.mortgageOfferByUserIncome(user);
		assertNotNull(retriveOffer);
		assertEquals(1, retriveOffer.size());
	}

	@Test
	public void testMortgageOfferByUserIncomeLessThanFiftyThousand() {
		user.setExistingEmi("12000");
		user.setFamilyIncome("70000");
		user.setOtherIncome("20000");
		user.setSalary("5000");

		List<String> retriveOffer = mortgageService.mortgageOfferByUserIncome(user);
		assertNotNull(retriveOffer);
		assertEquals(2, retriveOffer.size());
	}

	@Test(expected = NumberFormatException.class)
	public void testMortgageOfferByUserInvalidIncome() {
		user.setExistingEmi("120000000000000");
		user.setFamilyIncome("7000");
		user.setOtherIncome("2000");
		user.setSalary("5000");
		mortgageService.mortgageOfferByUserIncome(user);
	}

	@Test
	public void testMortgageOfferByUserIncomeLessThanTwentyThousand() {
		user.setExistingEmi("1200");
		user.setFamilyIncome("7000");
		user.setOtherIncome("2000");
		user.setSalary("5000");

		List<String> retriveOffer = mortgageService.mortgageOfferByUserIncome(user);
		assertNotNull(retriveOffer);
		assertEquals(1, retriveOffer.size());
	}

	@Test
	public void testSaveUserOfferDetailsValid() {
		userOffer.setId(11);;
		userOffer.setOfferid(22);
		userOffer.setUserid(33);
		String retriveOffer = mortgageService.saveUserOfferDetails(userOffer);
		assertNotNull(retriveOffer);
	}
	
	@Test
	public void testoffersValid() {
		offer.setId(11);
		offer.setEmi("40571");
		offer.setIncome("550000");
		offer.setLoanAmount("5000000");
		offer.setTenure("15yrs");
		offer.setRateOfInterest("9%");
		
		String retriveOffer = mortgageService.offers(offer);
		assertEquals("Success", retriveOffer);
	}


}
