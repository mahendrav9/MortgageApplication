package com.example.MortgageApplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

/**
 * 
 * MortgageApplication
 *
 */
@SpringBootApplication
@ComponentScan("com.mortgage.*")
@EntityScan("com.mortgage.domain")
public class MortgageApplication {

	public static void main(String[] args) {
		SpringApplication.run(MortgageApplication.class, args);
	}
}
