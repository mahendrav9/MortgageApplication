package com.mortgage.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import com.mortgage.controller.MortgageController;
import com.mortgage.dao.MortgageDao;
import com.mortgage.domain.Offer;
import com.mortgage.domain.User;
import com.mortgage.domain.UserOffer;
import com.mortgage.service.MortgageService;
import com.mortgage.utils.MortgageConstant;

/**
 * 
 * MortgageServiceImpl
 *
 */
@Service
public class MortgageServiceImpl implements MortgageService {

	static final Logger logger = LoggerFactory.getLogger(MortgageController.class);

	@Autowired
	MortgageDao mortgageDao;

	@Override
	public User findUserDetails(String username, String password) {
		User user = null;
		try {
			user = mortgageDao.findUserDetails(username, password);
		} catch (DataAccessException dae) {
			logger.error("MortgageController...findUserDetails", dae);
		}
		return user;

	}

	@Override
	public List<String> mortgageOfferByUserIncome(User user) {
		int totalIncome = 0;
		try {

			totalIncome = ((Integer.parseInt(user.getSalary()) + Integer.parseInt(user.getOtherIncome())
					+ Integer.parseInt(user.getFamilyIncome())) - Integer.parseInt(user.getExistingEmi())) * 40 / 100;
		} catch (NumberFormatException nfe) {
			logger.error("MortgageController...mortgageOfferByUserIncome", nfe);
			throw new NumberFormatException();

		}

		List<String> offers = new ArrayList<>();

		if (totalIncome > MortgageConstant.FIFTY_THOUSAND) {
			offers.add(MortgageConstant.FIFTY_LAKHS);
		} else if (totalIncome < MortgageConstant.TWENTY_FIVE_THOUSAND) {
			offers.add(MortgageConstant.TEN_LAKHS);
		} else if (totalIncome < MortgageConstant.FIFTY_THOUSAND) {
			offers.add(MortgageConstant.TWENTY_LAKHS);
			offers.add(MortgageConstant.TEN_LAKHS);
		}

		return offers;

	}

	@Override
	public String saveUserOfferDetails(UserOffer userOffer) {
		String status = "";
		try {
			mortgageDao.saveUserOfferDetails(userOffer);
			status = MortgageConstant.SUCCESS;

		} catch (DataAccessException dae) {
			status = MortgageConstant.FAIL;
			logger.error("MortgageController...saveUserOfferDetails", dae);
		}
		
		return status;

	}

	@Override
	public String offers(Offer offer) {
		String message = "";
		try {
			mortgageDao.offers(offer);
			message = MortgageConstant.SUCCESS;

		} catch (DataAccessException dae) {
			message = MortgageConstant.FAIL;
			logger.error("MortgageController...offers", dae);
		}
		
		return message;

	}
}
