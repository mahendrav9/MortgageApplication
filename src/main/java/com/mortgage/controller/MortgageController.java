package com.mortgage.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mortgage.domain.Offer;
import com.mortgage.domain.User;
import com.mortgage.domain.UserOffer;
import com.mortgage.service.MortgageService;

/**
 * 
 * 
 * MortgageController
 */
@RestController
@RequestMapping("/")
@CrossOrigin("*")
public class MortgageController {

	static final Logger logger = LoggerFactory.getLogger(MortgageController.class);

	@Autowired
	private MortgageService mortgageService;

	@GetMapping
	public ResponseEntity<String> getUser() {
		return new ResponseEntity<>("hello", HttpStatus.FOUND);
	}

	@GetMapping("user/{username}/{password}")
	public ResponseEntity<User> getUserByLoginCredentials(@PathVariable("username") String username,
			@PathVariable("password") String password) {

		User user = mortgageService.findUserDetails(username, password);
		if (user == null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<>(mortgageService.findUserDetails(username, password), HttpStatus.FOUND);
	}

	@PostMapping("/offerPerUser")
	public ResponseEntity<List<String>> mortgageOfferByUserIncome(@Valid @RequestBody User user) {
		return new ResponseEntity<>(mortgageService.mortgageOfferByUserIncome(user), HttpStatus.OK);

	}

	@PostMapping("/userofferdetails")
	public ResponseEntity<String> createUserOffer(@Valid @RequestBody UserOffer userOffer) {
		return new ResponseEntity<>(mortgageService.saveUserOfferDetails(userOffer), HttpStatus.CREATED);

	}

	@PostMapping("/offers")
	public ResponseEntity<String> offers(@RequestBody Offer offer) {
		return new ResponseEntity<>(mortgageService.offers(offer), HttpStatus.CREATED);

	}

}
