package com.mortgage.utils;

public final class MortgageConstant {

	public static final String FAIL = "Fail";
	public static final String SUCCESS = "Success";
	public static final int FIFTY_THOUSAND = 50000;
	public static final int TWENTY_FIVE_THOUSAND = 25000;
	public static final String TEN_LAKHS = "1000000Lakhs, Rate if Interest 9%, Tenure 15 Yrs, EMI 10143";
	public static final String FIFTY_LAKHS = "5000000Lakhs, Rate if Interest 9%, Tenure 15 Yrs, EMI 40571";
	public static final String TWENTY_LAKHS = "2000000Lakhs, Rate if Interest 9%, Tenure 15 Yrs, EMI 20285";

	private MortgageConstant() {
		// default constructor to hide
	}

}