package com.mortgage.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.mortgage.domain.UserOffer;

public interface UserOfferRepository extends JpaRepository<UserOffer, Long> {

}
