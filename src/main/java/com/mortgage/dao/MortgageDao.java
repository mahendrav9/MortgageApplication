package com.mortgage.dao;

import com.mortgage.domain.Offer;
import com.mortgage.domain.User;
import com.mortgage.domain.UserOffer;

/**
 * 
 * MortgageDao
 *
 */
public interface MortgageDao {

	User findUserDetails(String username, String password);

	void saveUserOfferDetails(UserOffer userOffer);

	void offers(Offer offer);

}
