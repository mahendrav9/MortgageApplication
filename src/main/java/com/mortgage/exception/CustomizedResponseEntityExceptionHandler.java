package com.example.mortgageappdemo.exception;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestController
@ControllerAdvice
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	static final Logger logger = LoggerFactory.getLogger(CustomizedResponseEntityExceptionHandler.class);
	
  @ExceptionHandler(Exception.class)
  public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
    ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getMessage(),
        request.getDescription(false), request.getContextPath());
    return new ResponseEntity(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(UserNotFoundException.class)
  public final ResponseEntity<Object> handleUserNotFoundException(UserNotFoundException ex, WebRequest request) {
    ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getLocalizedMessage(),
        request.getDescription(false),request.getContextPath());
	  
    return new ResponseEntity(exceptionResponse, HttpStatus.NOT_FOUND);
  }

  //it will handle validation failed
  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
	  //Only generalized messages.
    /*ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), "Validation Failed",
        ex.getBindingResult().toString());*/
	 
	  //Only default messages we can retrieve
	 /* List<String> details = new ArrayList<>();
      for(ObjectError error : ex.getBindingResult().getAllErrors()) {
          details.add(error.getDefaultMessage());
      }*/
     
	  logger.info("handleMethodArgumentNotValid...."+request.getContextPath()+"..."+request.getSessionId()+"..."+request.getLocale());
	  
		BindingResult bindingResult = ex.getBindingResult();

	    List<ApiFieldError> apiFieldErrors = bindingResult
            .getFieldErrors()
            .stream()
            .map(fieldError -> new ApiFieldError(
            		new Date(),
                    fieldError.getField(),
                    fieldError.getCode(),
                    fieldError.getRejectedValue().toString(),
                    fieldError.getDefaultMessage().toString())
            ).collect(Collectors.toList());
	    
	    
	    List<ApiGlobalError> apiGlobalErrors = bindingResult
                .getGlobalErrors()
                .stream()
                .map(globalError -> new ApiGlobalError(
                        globalError.getCode())
                )
                .collect(Collectors.toList());
	   
	    ApiErrorsView apiErrorsView = new ApiErrorsView(apiFieldErrors, apiGlobalErrors);
    
    return new ResponseEntity(apiErrorsView, HttpStatus.BAD_REQUEST);
  } 
  
  // it will handle no suitable handler found in controller
  @Override
  protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
	 
	  logger.info("handleNoHandlerFoundException...."+request.getContextPath()+"..."+request.getSessionId()+"..."+request.getLocale());
	  
				ApiFieldError apiFieldErrors = new ApiFieldError(
	            		new Date(),
	            		ex.getHttpMethod(),
	            		ex.getLocalizedMessage(),
	                    ex.getRequestURL(),
	                    ex.getMessage());	           

    return new ResponseEntity(apiFieldErrors, HttpStatus.NOT_FOUND);
  }
}