package com.mortgage.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@NamedQuery(name = "User.findAll", query = "SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@NotNull
	private int userid;

	private String address;

	private String dob;

	@Column(name = "existing_emi")
	@NotNull
	private String existingEmi;

	@Column(name = "family_income")
	@NotNull
	private String familyIncome;

	@Column(name = "full_name")
	private String fullName;

	private String gender;

	@Column(name = "other_income")
	@NotNull
	private String otherIncome;

	private String pan;

	@NotNull
	private String salary;

	private String username;

	private String password;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user")
	private List<PropertyDetail> propertyDetail;

	public List<PropertyDetail> getPropertyDetail() {
		return propertyDetail;
	}

	public void setPropertyDetail(List<PropertyDetail> propertyDetail) {
		this.propertyDetail = propertyDetail;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public User() {
		// Do nothing because of default constructor.
	}

	public int getUserid() {
		return this.userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getDob() {
		return this.dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getExistingEmi() {
		return this.existingEmi;
	}

	public void setExistingEmi(String existingEmi) {
		this.existingEmi = existingEmi;
	}

	public String getFamilyIncome() {
		return this.familyIncome;
	}

	public void setFamilyIncome(String familyIncome) {
		this.familyIncome = familyIncome;
	}

	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getOtherIncome() {
		return this.otherIncome;
	}

	public void setOtherIncome(String otherIncome) {
		this.otherIncome = otherIncome;
	}

	public String getPan() {
		return this.pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getSalary() {
		return this.salary;
	}

	public void setSalary(String salary) {
		this.salary = salary;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}