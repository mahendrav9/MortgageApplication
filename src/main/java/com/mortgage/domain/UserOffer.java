package com.mortgage.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the user_offer database table.
 * 
 */
@Entity
@Table(name = "user_offer")
@NamedQuery(name = "UserOffer.findAll", query = "SELECT u FROM UserOffer u")
public class UserOffer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private int offerid;

	private int userid;

	public UserOffer() {
		// Do nothing because of default constructor.
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getOfferid() {
		return this.offerid;
	}

	public void setOfferid(int offerid) {
		this.offerid = offerid;
	}

	public int getUserid() {
		return this.userid;
	}

	public void setUserid(int userid) {
		this.userid = userid;
	}

}