package com.mortgage.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the offer database table.
 * 
 */
@Entity
@NamedQuery(name = "Offer.findAll", query = "SELECT o FROM Offer o")
public class Offer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String emi;

	private String income;

	@Column(name = "loan_amount")
	private String loanAmount;

	@Column(name = "rate_of_interest")
	private String rateOfInterest;

	private String tenure;

	public Offer() {
		// Do nothing because of default constructor.
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmi() {
		return this.emi;
	}

	public void setEmi(String emi) {
		this.emi = emi;
	}

	public String getIncome() {
		return this.income;
	}

	public void setIncome(String income) {
		this.income = income;
	}

	public String getLoanAmount() {
		return this.loanAmount;
	}

	public void setLoanAmount(String loanAmount) {
		this.loanAmount = loanAmount;
	}

	public String getRateOfInterest() {
		return this.rateOfInterest;
	}

	public void setRateOfInterest(String rateOfInterest) {
		this.rateOfInterest = rateOfInterest;
	}

	public String getTenure() {
		return this.tenure;
	}

	public void setTenure(String tenure) {
		this.tenure = tenure;
	}

}