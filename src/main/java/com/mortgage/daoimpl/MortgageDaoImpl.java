package com.mortgage.daoimpl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.mortgage.controller.MortgageController;
import com.mortgage.dao.MortgageDao;
import com.mortgage.domain.Offer;
import com.mortgage.domain.User;
import com.mortgage.domain.UserOffer;

/**
 * 
 * MortgageDaoImpl
 *
 */
@Repository
public class MortgageDaoImpl implements MortgageDao {

	static final Logger logger = LoggerFactory.getLogger(MortgageController.class);

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public User findUserDetails(String username, String password) {

		Query q = entityManager.createNativeQuery(
				"SELECT * FROM User user WHERE user.username = :username and user.password = :password");
		q.setParameter("username", username);
		q.setParameter("password", password);
		return (User) q.getSingleResult();

	}

	@Override
	public void saveUserOfferDetails(UserOffer useroffer) {
		entityManager.persist(useroffer);
	}

	@Override
	public void offers(Offer offer) {
		entityManager.persist(offer);
	}

}
