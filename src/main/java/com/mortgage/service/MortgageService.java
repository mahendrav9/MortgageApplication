package com.mortgage.service;

import java.util.List;

import com.mortgage.domain.Offer;
import com.mortgage.domain.User;
import com.mortgage.domain.UserOffer;

public interface MortgageService {

	public User findUserDetails(String username, String password);

	List<String> mortgageOfferByUserIncome(User user);

	String saveUserOfferDetails(UserOffer userOffer);

	String offers(Offer offer);

}
